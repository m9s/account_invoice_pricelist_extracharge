# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.pyson import Eval, Bool
from trytond.pool import Pool


class Pricelist(ModelSQL, ModelView):
    _name = 'pricelist.pricelist'

    def __init__(self):
        super(Pricelist, self).__init__()
        self._error_messages.update({
            'company_not_found': 'Company not found in context!',
            'no_time': 'Start and end time are required to calculate '
                'extra charge!'
            })

    def compute_billing_lines(self, product, quantity=1, from_unit=False,
            pricelist=False, start_time=False, end_time=False,
            price_type='default', currency=False):
        pool = Pool()
        product_obj = pool.get('product.product')
        uom_obj = pool.get('product.uom')
        company_obj = pool.get('company.company')
        version_obj = pool.get('pricelist.version')
        item_obj = pool.get('pricelist.item')
        currency_obj = pool.get('currency.currency')
        extra_charge_rule_obj = pool.get('pricelist.extra_charge.rule')

        if isinstance(pricelist, (int, long)):
            pricelist = self.browse(pricelist)

        if from_unit and isinstance(from_unit, (int, long)):
            from_unit = uom_obj.browse(from_unit)

        if isinstance(product, (int, long)):
            product = product_obj.browse(product)

        if not currency:
            company_id = Transaction().context.get('company')
            if not company_id:
                self.raise_user_error('company_not_found')

            company = company_obj.browse(company_id)
            currency = company.currency

        res = []
        if not pricelist:
            return res

        version_ids = version_obj.get_versions(pricelist.id,
                date=start_time.date())
        versions = [version_ids['default'], version_ids['standard']]

        for version_id in versions:
            args = self._get_product_args(product)
            args1 = ['AND']
            args1.append(args)
            args1.append(('pricelist_version.id', '=', version_id))
            item_ids = item_obj.search(args1, order=[('sequence', 'ASC')],
                    limit=1)
            if not item_ids:
                continue
            item = item_obj.browse(item_ids[0])

            from_unit = from_unit or product.default_uom
            to_unit = item.unit or product.default_uom

            quantity = uom_obj.compute_qty(from_unit, quantity,
                    to_unit, round=True)
            if quantity < item.minimum_quantity:
                quantity = item.minimum_quantity

            price = False
            price = item_obj.get_item_price(item, product.id, quantity)
            if not price:
                continue
            with Transaction().set_context(date=start_time.date()):
                price = currency_obj.compute(
                    item.pricelist_version.pricelist.currency.id, price,
                    currency.id, round=False)

            line = {}
            line['date'] = start_time.date()
            line['unit'] = to_unit
            line['quantity'] = quantity
            line['price'] = price
            line['description'] = product.name
            res.append(line)

            if item.extra_charge:
                if not start_time or not end_time:
                    self.raise_user_error('no_time')

                # Extra charge unit is for now always from_unit
                # So price of pricelist item must be computed to from_unit
                extra_price = uom_obj.compute_price(to_unit, price, from_unit)
                extra_charge_lines = extra_charge_rule_obj.apply(
                        item.extra_charge, extra_price, start_time, end_time)
                for line in extra_charge_lines:
                    line['description'] = (product.name + ' ' +
                            line.get('description_suffix', ''))
                    line['quantity'] = uom_obj.compute_qty(from_unit,
                            line['quantity'], from_unit, round=True)
                    line['unit'] = from_unit
                res.extend(extra_charge_lines)
            break
        return res

Pricelist()


class PricelistItem(ModelSQL, ModelView):
    _name = 'pricelist.item'

    unit = fields.Many2One('product.uom', 'Unit',
            depends=['invoice_line', 'product', 'product_category'],
            domain=[
                ('category', '=',
                    (Eval('product'), 'product.default_uom.category')),
            ],
            context={
                'category': (Eval('product'), 'product.default_uom.category'),
            }, help='Set a unit if it shall be different from the default ' \
            'unit of measure of the product', states={
                'invisible': Bool(Eval('product_category'))
            })
    extra_charge = fields.Many2One('pricelist.extra_charge.rule',
            'Extra Charge')
    minimum_quantity = fields.Float('Minimum Quantity',
            help="The minimum quantity to bill. This quantity is based on the "
                "unit of measure defined in field 'Unit' resp. the default on "
                "the product.")

    def on_change_product_category(self, vals):
        category_obj = Pool().get('product.category')
        res = {}
        if vals.get('product_category'):
            category = category_obj.browse(vals['product_category'])
            res['name'] = category.name
        res['product'] = False
        res['product_template'] = False
        res['unit'] = False
        return res

PricelistItem()
