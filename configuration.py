from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.pyson import Not, Bool, Eval


class Configuration(ModelSingleton, ModelView, ModelSQL):
    'Pricelist Configuration'
    _name = "pricelist.configuration"
    _description = __doc__

    holiday_calendar = fields.Property(fields.Many2One('calendar.calendar',
        'Public Holiday Calendar', required=True))

Configuration()
