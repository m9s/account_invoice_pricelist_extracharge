# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Pricelist Extracharge',
    'name_de_DE': 'Fakturierung Preislisten Zuschläge',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Provides the possibility to define rules for extra charges on holiday
      calendars.
    ''',
    'description_de_DE': '''
    - Stellt die Möglichkeit der Definition von Regeln für Zuschläge mit
      Feiertagskalendern zur Verfügung.
    ''',
    'depends': [
        'account_invoice_pricelist',
        'calendar_template'
    ],
    'xml': [
        'configuration.xml',
        'extracharge.xml',
        'pricelist.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
