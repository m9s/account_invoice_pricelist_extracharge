# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import datetime
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Not, Equal
from trytond.pool import Pool


_TRANSFORM_WEEKDAY = {
    0: 'monday',
    1: 'tuesday',
    2: 'wednesday',
    3: 'thursday',
    4: 'friday',
    5: 'saturday',
    6: 'sunday'
}


class ExtraChargeRule(ModelSQL, ModelView):
    'Extra Charge Rule'
    _name = 'pricelist.extra_charge.rule'
    _description = __doc__

    name = fields.Char('Name', required=True)
    lines = fields.One2Many('pricelist.extra_charge.rule.line', 'rule',
            'Lines', required=True)

    def apply(self, rule, price, start_time, end_time):
        if not rule:
            return False

        if isinstance(rule, (int, long)) and rule:
            rule = self.browse(rule)

        time_per_days = self._get_time_per_day(start_time, end_time)

        res = []
        for item in time_per_days:
            res.extend(self._get_extra_charge_data(item, price))
        return res

    def _get_time_per_day(self, start_time, end_time):
        config_obj = Pool().get('pricelist.configuration')
        calendar_obj = Pool().get('calendar.calendar')

        calendar_id = config_obj.read(1, ['holiday_calendar'])[
                'holiday_calendar']
        time_left = True
        time_per_day_start = start_time

        res = []
        while time_left:
            time_per_day_end = datetime.datetime.combine(
                    time_per_day_start.date(), datetime.time.max)
            if end_time <= time_per_day_end:
                time_per_day_end = end_time
                time_left = False
            is_holiday = calendar_obj.check_event_match(time_per_day_start,
                    calendar_id)
            res.append({
                    'date': time_per_day_start.date(),
                    'weekday': time_per_day_start.weekday(),
                    'start_time': time_per_day_start,
                    'end_time': time_per_day_end,
                    'is_holiday': is_holiday
            })
            time_per_day_start = datetime.datetime.combine(
                    time_per_day_start.date() + datetime.timedelta(days=1),
                    datetime.time.min)
        return res

    def _get_extra_charge_data(self, item, price):
        rule_line_obj = Pool().get('pricelist.extra_charge.rule.line')

        start_time_hour = (item['start_time'].hour < 10 and
                str('0' + str(item['start_time'].hour)) or
                str(item['start_time'].hour))
        start_time_minute = (item['start_time'].minute < 10 and
                str('0' + str(item['start_time'].minute)) or
                str(item['start_time'].minute))
        end_time_hour = (item['end_time'].hour < 10 and
                str('0' + str(item['end_time'].hour)) or
                str(item['end_time'].hour))
        end_time_minute = (item['end_time'].minute < 10 and
                str('0' + str(item['end_time'].minute)) or
                str(item['end_time'].minute))

        rule_lines = []
        if item['is_holiday']:
            args = [
                ('is_holiday', '=', item['is_holiday'])
            ]
            rule_lines = rule_line_obj.search_read(args, order=[
                ('start_time_hours', 'ASC'),
                ('start_time_minutes', 'ASC')
            ], fields_names=[
                'end_time_hours',
                'end_time_minutes',
                'start_time_hours',
                'start_time_minutes',
                'name',
                'markup',
                'type',
                'amount_fix'
            ])
        if not rule_lines:
            args = [
                (_TRANSFORM_WEEKDAY[item['weekday']], '=', True),
            ]
            rule_lines = rule_line_obj.search_read(args, order=[
                ('start_time_hours', 'ASC'),
                ('start_time_minutes', 'ASC')
            ], fields_names=[
                'end_time_hours',
                'end_time_minutes',
                'start_time_hours',
                'start_time_minutes',
                'name',
                'markup',
                'type',
                'amount_fix'
            ])

        res = []
        if not rule_lines:
            return res

        computation_complete = False
        for rule_line in rule_lines:
            start_time_rule = datetime.time(
                    int(rule_line['start_time_hours']),
                    int(rule_line['start_time_minutes'])
                )
            if rule_line['end_time_hours'] == '24':
                end_time_rule = datetime.time.max
            else:
                end_time_rule = datetime.time(
                        int(rule_line['end_time_hours']),
                        int(rule_line['end_time_minutes'])
                    )
            start_time_item = datetime.time(
                    int(start_time_hour),
                    int(start_time_minute)
                )
            end_time_item = datetime.time(
                    int(end_time_hour),
                    int(end_time_minute)
                )
            vals = []

            if end_time_item < start_time_rule:
                break
            elif start_time_item >= end_time_rule:
                continue
            elif start_time_item < start_time_rule:
                if end_time_item > start_time_rule:
                    start_time_hour = rule_line['start_time_hours']
                    start_time_minute = rule_line['start_time_minutes']

                    if end_time_item <= end_time_rule:
                        vals.append({
                            'start_time_hour': start_time_hour,
                            'start_time_minute': start_time_minute,
                            'end_time_hour': end_time_hour,
                            'end_time_minute': end_time_minute,
                            'description_suffix': rule_line['name']
                        })
                        computation_complete = True
                    else:
                        vals.append({
                            'start_time_hour': start_time_hour,
                            'start_time_minute': start_time_minute,
                            'end_time_hour': rule_line['end_time_hours'],
                            'end_time_minute': rule_line['end_time_minutes'],
                            'description_suffix': rule_line['name']
                        })
                        start_time_hour = rule_line['end_time_hours']
                        start_time_minute = rule_line['end_time_minutes']
            elif start_time_item < end_time_rule:
                if end_time_item <= end_time_rule:
                    vals.append({
                        'start_time_hour': start_time_hour,
                        'start_time_minute': start_time_minute,
                        'end_time_hour': end_time_hour,
                        'end_time_minute': end_time_minute,
                        'description_suffix': rule_line['name']
                    })
                    computation_complete = True
                else:
                    vals.append({
                        'start_time_hour': start_time_hour,
                        'start_time_minute': start_time_minute,
                        'end_time_hour': rule_line['end_time_hours'],
                        'end_time_minute': rule_line['end_time_minutes'],
                        'description_suffix': rule_line['name']
                    })
                    start_time_hour = rule_line['end_time_hours']
                    start_time_minute = rule_line['end_time_minutes']

            if vals:
                for value in vals:
                    if rule_line['type'] == 'fix':
                        unit_price = rule_line['amount_fix']
                    elif rule_line['type'] == 'markup':
                        unit_price = price * (rule_line['markup'] / 100)
                    quantity = self._get_quantity(value)
                    res.append({
                        'date': item['date'],
                        'price': unit_price,
                        'quantity': quantity,
                        'description_suffix': value['description_suffix']
                    })
        return res

    def _get_quantity(self, vals):
        start_time = datetime.timedelta(
                hours=int(vals['start_time_hour']),
                minutes=int(vals['start_time_minute'])
        )
        end_time = datetime.timedelta(
                hours=int(vals['end_time_hour']),
                minutes=int(vals['end_time_minute'])
        )
        if vals['end_time_minute'] == '59':
            end_time = end_time + datetime.timedelta(minutes=1)

        res = False
        td = end_time - start_time
        if td > datetime.timedelta(days=0):
            td_seconds = (td.microseconds +
                    (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6
            res = round(float(td_seconds) / float(3600), 2)
        return res

ExtraChargeRule()


class ExtraChargeRuleLine(ModelSQL, ModelView):
    'Extra Charge Rule Line'
    _name = 'pricelist.extra_charge.rule.line'
    _description = __doc__
    _rec_name = 'extra_charge'

    rule = fields.Many2One('pricelist.extra_charge.rule', 'Extra Charge',
            required=True)
    start_time_hours = fields.Selection('get_hours', 'Start Time Hours',
            states={
                'required': True
            })
    start_time_minutes = fields.Selection('get_minutes', 'Start Time Minutes',
            states={
                'required': True
            })
    end_time_hours = fields.Selection('get_hours', 'End Time Hours',
            states={
                'required': True
            })
    end_time_minutes = fields.Selection('get_minutes', 'End Time Minutes',
            states={
                'required': True
            })
    monday = fields.Boolean('Monday')
    tuesday = fields.Boolean('Tuesday')
    wednesday = fields.Boolean('Wednesday')
    thursday = fields.Boolean('Thursday')
    friday = fields.Boolean('Friday')
    saturday = fields.Boolean('Saturday')
    sunday = fields.Boolean('Sunday')
    is_holiday = fields.Boolean('Public Holiday')
    sequence = fields.Integer('Sequence', required=True)
    type = fields.Selection([
            ('markup', 'Markup'),
            ('fix', 'Fix')
            ], 'Type', required=True)
    markup = fields.Numeric('Markup Percentage', digits=(16, 8),
            states={
                'invisible': Not(Equal(Eval('type'), 'markup')),
                'required': Equal(Eval('type'), 'markup'),
            }, depends=['type'])
    amount_fix = fields.Numeric('Fix Price', digits=(16, 4),
            states={
                'invisible': Not(Equal(Eval('type'), 'fix')),
                'required': Equal(Eval('type'), 'fix'),
            }, depends=['type'])
    name = fields.Char('Name', required=True, translate=True, loading='lazy')

    def __init__(self):
        super(ExtraChargeRuleLine, self).__init__()
        self._order.insert(0, ('rule', 'ASC'))
        self._order.insert(0, ('sequence', 'ASC'))

    def default_sequence(self):
        return 10

    def default_start_time_minutes(self):
        return '00'

    def default_end_time_minutes(self):
        return '00'

    def default_type(self):
        return 'markup'

    def get_hours(self):
        res = []
        for i in range(25):
            if i < 10:
                h = str('0' + str(i))
            else:
                h = str(i)
            res.append((h, h))
        return res

    def get_minutes(self):
        res = [('00', '00'), ('15', '15'), ('30', '30'), ('45', '45')]
        return res

ExtraChargeRuleLine()
